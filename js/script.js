let x = prompt('\nEnter a number:')

while(x === null || x <= 0 || x < 0 || Number.isNaN(+x) || x.trim() === ''){
  x = prompt('Error! \nEnter a positive integer number:', x)
}

x = + x

getFactorial = (x) => x <= 2 ? x : x * getFactorial(x-1, x-2)

alert(`Factorial of ${x} = ${getFactorial(x)}`)
